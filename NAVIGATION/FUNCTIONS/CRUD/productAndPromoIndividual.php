<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>promocion</title>
            <!-- Bootstrap -->
        <link href="../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
        <!-- Custom Theme Style -->
        <link href="../../DESING/CSS/custom.min.css" rel="stylesheet">
        <script src="../INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
    </head>
    <style>
        .img-fluid {
            max-width: 40%;
            height: auto;
        }
        .container{
            margin-top:10%;
        }
    </style>
    <body>

<?php 
    $tipo = $_POST['tipo'];
    if ($tipo == '1') {
        $idPromocion = $_POST['promo'];
        if (isset($idPromocion)) {
    
            ?>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                        <div class="alert alert-success alert-dismissible " role="alert">
                                <div class="text-center">   
                                    <strong>Promocion Agregada</strong> La promocion fue agregada correctamente, importante cerrar la pestaña emergente.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <div class="text-center">
                                <img src="../../DESING/IMG/CHULO.png" class="img-fluid" alt="Responsive image">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                </div>
            <?php
        }
    }else if ($tipo == '2') {
        $idProducto = $_POST['producto'];
        if (isset($idProducto)) {
    
            ?>
            <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                        <div class="alert alert-success alert-dismissible " role="alert">
                                <div class="text-center">   
                                    <strong>Producto Agregado</strong> El producto fue agregada correctamente, importante cerrar la pestaña emergente.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <div class="text-center">
                                <img src="../../DESING/IMG/CHULO.png" class="img-fluid" alt="Responsive image">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                </div>
            <?php
        }
    }
?>
<!-- Bootstrap -->
<script src="../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
    </body>
    </html>
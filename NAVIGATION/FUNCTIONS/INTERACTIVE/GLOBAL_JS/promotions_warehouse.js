$(document).ready(function () 
{
    function consultarAlmacen() 
    {

        var idAlmacen = $('#Almacenes').val();

        $.ajax(
        {
            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/almacenes_promotions.php',

            data:
            {
                idAlmacen: idAlmacen
            },
            type: 'post',
                beforesend: function () {
            },

            success: function (data)
            {
                $('#returnConsultarAlmacen').html(data);
            }
        });
    }

    $('#BuscarAlmacen').click(function () {
         
        consultarAlmacen();

    });
});

$(function () 
    {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    });
});
           
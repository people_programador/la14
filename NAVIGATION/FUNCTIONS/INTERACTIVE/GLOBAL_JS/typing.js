$(function () 
    {
    //Initialize Select2 Elements
    $('.select2_v1').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    });
});

$(function () 
    {
    //Initialize Select2 Elements
    $('.select2_v2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    });
});

$(document).ready(function () 
{
    function consultarAlmacen(almacen,producto)
    {
        
        if (almacen == "" && producto == "") {
       
        }else if (almacen != "" && producto == "") {

            var tipo = "1"; //consulta por almacen
            almacenAndProducts(tipo,almacen,producto);
            
        }else if (almacen == "" && producto != "") {

            var tipo = "2"; //consulta por producto
            almacenAndProducts(tipo,almacen,producto);
            
        }else if (almacen != "" && producto != "") {

            var tipo = "3"; //consulta por almacen y producto
            almacenAndProducts(tipo,almacen,producto);
            
        }  
    }

    function almacenAndProducts(tipo,almacen,producto) 
    {
        $.ajax(
        {
            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/interactive_typing.php',

            data:
            {
                tipo: tipo,
                almacen: almacen,
                producto: producto
            },
            type: 'post',
                beforesend: function () {
            },

            success: function (data)
            {
                $('#returnAlmacenAndProducts').html(data);
               
            }
        });
    }

    function consultarPromociones(promoAlmacen) 
    {
        $.ajax(
        {
            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/interactive_typing.php',

            data:
            {
                promoAlmacen: promoAlmacen
            },
            type: 'post',
                beforesend: function () {
            },

            success: function (data)
            {
                $('#returnConsultarPromociones').html(data);
               
            }
        });
    }


    function agregarProductos()
    {
        var productoNuevo = "productoNuevo";
        $.ajax(
            {
                url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/interactive_typing.php',
    
                data:
                {
                    productoNuevo: productoNuevo
                },
                type: 'post',
                    beforesend: function () {
                },
    
                success: function (data)
                {
                    $('#returnAgregarProductos').html(data);
                   
                }
            });
    }

    $('#buscarProducto').change(function () {

        var almacen = "";
        var producto = "";

        almacen = $('#buscarAlmacen').val();
        producto = $('#buscarProducto').val();

        consultarAlmacen(almacen,producto);

    });

    $('#buscarAlmacen').change(function () {

        var almacen = "";
        var producto = "";
      
        almacen = $('#buscarAlmacen').val();
        producto = $('#buscarProducto').val();

        consultarAlmacen(almacen,producto);

    });

    $('#Productosypromo').click(function () {

        var almacen = "";
        var producto = "";
        

        almacen = $('#buscarAlmacen').val();
        producto = $('#buscarProducto').val();

        consultarAlmacen(almacen,producto);

    });

    $('#buscarProAlmacen').click(function () {

        var promoAlmacen = "";

        promoAlmacen = $('#promoAlmacenes').val(); 
        
        consultarPromociones(promoAlmacen);

    });

    $('#agregarProducto').click(function () {
        agregarProductos();
    }); 
    

})
         
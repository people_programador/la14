<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Almcenes</title>
        <!-- PNotify -->
        <link href="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.css" rel="stylesheet">
        <link href="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
        <link href="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">
    </head>
    <?php 
    require ('../../../CONNECTION/SECURITY/conex.php'); 
    ?>
    <body>

        <?php 
        $idAlmacen = $_POST['idAlmacen'];

        $promocionesDelAlamcen=mysqli_query($conex,"SELECT * FROM promocion WHERE id_almacen = '$idAlmacen';");   
        $countPromociones = mysqli_num_rows($promocionesDelAlamcen);

        $infoAlmacen=mysqli_query($conex,"SELECT * FROM almacen WHERE id_almacen = '$idAlmacen';");
        while ($almacen =(mysqli_fetch_array($infoAlmacen))){
            $nombreAlmacen = $almacen['subnombre'];
            $descripcionAlmacen = $almacen['descripcion'];
            $ciudad = $almacen['ciudad'];
            $direccion = $almacen['direccion'];
            $barrio = $almacen['barrio'];
            $telefono = $almacen['telefono'];
            $correo = $almacen['correo'];
            $imagen = $almacen['image'];
        }

        ?>
        <div class="col-md-4 col-sm-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Almacen <?php echo $nombreAlmacen?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                    <img style="width: 100%; display: block;" src="../../../DESING/IMG/<?php echo $imagen;?>" alt="image" />
                                    <div class="mask">
                                        <p>Almacen <?php echo $nombreAlmacen?></p>
                                        <div class="tools tools-bottom">
                                            <!-- //-->
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p><?php echo $descripcionAlmacen ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <p>Informacion del almacen</p>
                            <ul>
                                <li><?php echo $nombreAlmacen ?></li>
                                <li><?php echo $ciudad ?></li>
                                <li><?php echo $direccion ?></li>
                                <li><?php echo $barrio ?></li>
                                <li><?php echo $telefono ?></li>
                                <li><?php echo $correo ?></li>
                            </ul>
                        </div>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p><a id='demostracionMuestras' href="#">Demostracion y muestras</a></p>
                            </div>           
                        </div>
                    </div>      
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p>Cupones o Puntos</p>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p>Descuentos</p>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p>Productos gratuitos</p>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p>Combos</p>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>promocion</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>                
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="tile-stats">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="count">5</div>
                                <p>Especiales</p>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <input type="hidden" id="countPromociones" value="<?php echo $countPromociones ?>">
        <input type="hidden" id="nombreAlmacen" value="<?php echo $nombreAlmacen ?>">
        <!-- PNotify -->
        <script src="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.js"></script>
        <script src="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.buttons.js"></script>
        <script src="../../../DESING/BOOKSTORES/PACKAGE/pnotify/dist/pnotify.nonblock.js"></script>
        
        <!-- jQuery -->
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/alamacenes_promotions.js"></script>
        <!-- Custom Theme Scripts al final por que interfiere con jquery -->
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
    </body>
</html>

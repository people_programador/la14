<script>
              $(document).ready(function()
              {
                var viae=$('#VIAe').val();
                var dt_viae=$('#detalle_viae').val();
                $('#VIAe').change(function()
                {
                  dire();
                });
                
                $('#detalle_viae').change(function()
                {
                  dire();
                });
                $('#numeroe').change(function()
                {
                  dire();
                });
                $('#numero2e').change(function()
                {
                  dire();
                });
                $('#interiore').change(function()
                {
                  dire();		
                });
                $('#detalle_inte').change(function()
                {
                  dire();
                });
                $('#interior2e').change(function()
                {
                  dire();		
                });
                $('#detalle_int2e').change(function()
                {
                  dire();
                });
                $('#interior3e').change(function()
                {
                  dire();		
                });
                $('#detalle_int3e').change(function()
                {
                  dire();
                });                
              });
            </script>
			
			<div class="form-label-left input_mask">
			<div class="row">
                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left"  name="nombreE" id="nombreE"  placeholder="Nombres">
                                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left"  name="apellidoE" id="apellidoE" placeholder="Apellidos">
                                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left"  name="emailE" id="emailE" placeholder="Email">
                                      <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                    </div>
         </div>
		 <div class="row">
                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" name="documentoE" id="documentoE" placeholder="documento">
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left"  name="telefonoE" id="telefonoE" placeholder="Tel&eacute;fono">
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <select class="form-control has-feedback-left"  name="ciudadE" id="ciudadE" placeholder="Ciudad" >
									  <option selected value="">selecionar...</option>
                                        <?php
                                        $selectAlmacen=mysqli_query($conex,"SELECT * FROM ciudad where estado = '1' ");

                                        while ($almacen =(mysqli_fetch_array($selectAlmacen))) { ?>

                                        <option value="<?php echo $almacen['id_ciudad']; ?>"><?php echo $almacen['nombre']; ?></option>

                                        <?php 
                                        } 
                                        ?>
									  </select>
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>
									</div>
<div class="row">
                                    <div class="col-md-12 col-sm-12  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" placeholder="Direcci&oacute;n" name="DIRECCIONe" id="DIRECCIONe" disabled="disabled" />
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                      </div> 
									  <div class="row">
                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Via:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="VIAe" name="VIAe" class="form-control">
                                              <option value="">Seleccione...</option>
                                              <option>ANILLO VIAL</option>
                                              <option>AUTOPISTA</option>
                                              <option>AVENIDA</option>
                                              <option>BOULEVAR</option>
                                              <option>CALLE</option>
                                              <option>CALLEJON</option>
                                              <option>CARRERA</option>
                                              <option>CIRCUNVALAR</option>
                                              <option>CONDOMINIO</option>
                                              <option>DIAGONAL</option>
                                              <option>KILOMETRO</option>
                                              <option>LOTE</option>
                                              <option>SALIDA</option>
                                              <option>SECTOR</option>
                                              <option>TRANSVERSAL</option>
                                              <option>VEREDA</option>
                                              <option>VIA</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Via:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_viae" id="detalle_viae" type="text" maxlength="15" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">N&uacute;mero:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="numeroe" id="numeroe" type="text" maxlength="5" class="form-control"/>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">
                                          <label style="text-align: center;">-</label>
                                        </div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="numero2e" id="numero2e" type="text" maxlength="5" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interiore" name="interiore" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_inte" id="detalle_inte" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interior2e" name="interior2e" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_int2e" id="detalle_int2e" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interior3e" name="interior3e" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_int3e" id="detalle_int3e" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>
                                    </div>  </div>
                                    
                                    <div class="ln_solid"></div>
                                    
                                  </div>

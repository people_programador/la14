<?php
require ('../../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
require ('../../../CONNECTION/SECURITY/conex.php');
$tipo = $_POST['tipo']; //echo $tipo; 
$producto = $_POST['producto']; //echo $producto;
$almacen = $_POST['almacen']; //echo $almacen;
$promoAlmacen = $_POST['promoAlmacen']; //echo $promoAlmacen;
$productoNuevo = $_POST['productoNuevo']; //echo $productoNuevo;

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- datables -->
        <link href="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
        <title>typing</title>
    </head>
    <style>
        .media .profile_thumb {
            border: 1px solid;
            width: 20px;
            height: 20px;
            margin: 5px 10px 5px 0;
            border-radius: 50%;
            padding: 9px 12px;
        }
        #sizeFont{
            font-size:0.9em;
        }
    </style>
    <body>
        <?php

        if($tipo == '1')
        {
        ?>
        <div class="col-sm-12">
            <br><br>
            <div class="x_panel">   
                <div class="x_title">
                    <h2><strong>Promociones del almacen</strong> - Busqueda por almacen  </h2>  
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="card-box table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped table-bordered col-sm-11" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Agregar</th>
                                    <th>Producto</th>
                                    <th>marca</th>
                                    <th>descripcion</th>
                                    <th>referencia</th>
                                    <th>almacen</th>
                                    <th>valor</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php   $selectProducto = mysqli_query($conex,"SELECT A.id_producto AS idProducto, A.nombre_pro AS nombreProducto, A.marca AS marcaProducto, A.descripcion AS descripcionProducto, A.referencia_pro AS referenciaProducto, B.subnombre AS nombreAlmacen, A.valor AS valorProducto FROM producto AS A LEFT JOIN almacen AS B ON A.id_almacen = B.id_almacen WHERE A.estado = '1' "); 
                                while ($producto =(mysqli_fetch_array($selectProducto))) {
                                
                                $idProducto = $producto['idProducto'];
                                $nombreProducto = $producto['nombreProducto'];
                                $marcaProducto = $producto['marcaProducto'];
                                $descripcionProducto = $producto['descripcionProducto'];
                                $referenciaProducto = $producto['referenciaProducto'];
                                $nombreAlmacen = $producto['nombreAlmacen'];
                                $AccionProducto = "accion";    ?>

                                <tr role='row'>
                                    <td>
                                        <center>
                                        <button type="button" class="btn btn-outline-info btn-sm" onClick="javascript:ventanaSecundaria2('../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ventPoductAndPromo.php?xnfgtiyyjk=<?php echo base64_encode($idProducto)?>')">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        </center>
                                    </td>
                                    <td><?php echo $nombreProducto; ?></td>
                                    <td><?php echo $marcaProducto; ?></td>
                                    <td><?php echo $descripcionProducto; ?></td>
                                    <td><?php echo $referenciaProducto; ?></td>
                                    <td><?php echo $nombreAlmacen; ?></td>
                                    <td><?php echo $AccionProducto; ?></td>
                                </tr>   <?php  
                                }
                                ?>   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- scritp -->
        <script>
            $(document).ready(function () {
                $('#myTable').DataTable();
            });
        </script>
        <?php
        }
        else if($tipo == '2')
        {
        ?>
        <div class="col-sm-12"
             <br><br>
            <div class="x_panel">   
                <div class="x_title">
                    <h2><strong>Promociones del almacen</strong> - buqueda de productos en los almacen </h2>  
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="card-box table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped table-bordered col-sm-11" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Agregar</th>
                                    <th>Producto</th>
                                    <th>marca</th>
                                    <th>descripcion</th>
                                    <th>referencia</th>
                                    <th>almacen</th>
                                    <th>valor</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php   $selectProducto = mysqli_query($conex,"SELECT A.id_producto AS idProducto, A.nombre_pro AS nombreProducto, A.marca AS marcaProducto, A.descripcion AS descripcionProducto, A.referencia_pro AS referenciaProducto, B.subnombre AS nombreAlmacen, A.valor AS valorProducto FROM producto AS A LEFT JOIN almacen AS B ON A.id_almacen = B.id_almacen WHERE A.estado = '1' AND A.palabras_clave LIKE '%$producto%';"); 
                                while ($producto =(mysqli_fetch_array($selectProducto))) {

                                $idProducto = $producto['idProducto'];
                                $nombreProducto = $producto['nombreProducto'];
                                $marcaProducto = $producto['marcaProducto'];
                                $descripcionProducto = $producto['descripcionProducto'];
                                $referenciaProducto = $producto['referenciaProducto'];
                                $nombreAlmacen = $producto['nombreAlmacen'];
                                $AccionProducto = "accion";    ?>

                                <tr role='row'>
                                    <td>
                                        <center>
                                        <button type="button" class="btn btn-outline-info btn-sm" onClick="javascript:ventanaSecundaria2('../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ventPoductAndPromo.php?xnfgtiyyjk=<?php echo base64_encode($idProducto)?>')">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        </center>
                                    </td>
                                    <td><?php echo $nombreProducto; ?></td>
                                    <td><?php echo $marcaProducto; ?></td>
                                    <td><?php echo $descripcionProducto; ?></td>
                                    <td><?php echo $referenciaProducto; ?></td>
                                    <td><?php echo $nombreAlmacen; ?></td>
                                    <td><?php echo $AccionProducto; ?></td>
                                </tr>   <?php  
                                }
                                ?>   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- scritp -->
        <script>
            $(document).ready(function () {
                $('#myTable').DataTable();
            });
        </script>
        <?php
        }
        else if($tipo == '3')
        {        
        ?>
        <div class="col-sm-12">

            <br><br>
            <div class="x_panel">   
                <div class="x_title">
                    <h2><strong>Promociones del almacen</strong> - buqueda de productos por almacen </h2>  
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class=""><i>&nbsp;</i></a></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="card-box table-responsive">
                        <br>
                        <table id="myTable" class="table table-striped table-bordered col-sm-11" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Agregar</th>
                                    <th>Producto</th>
                                    <th>marca</th>
                                    <th>descripcion</th>
                                    <th>referencia</th>
                                    <th>almacen</th>
                                    <th>valor</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php   $selectProducto = mysqli_query($conex,"SELECT A.id_producto AS idProducto, A.nombre_pro AS nombreProducto, A.marca AS marcaProducto, A.descripcion AS descripcionProducto, A.referencia_pro AS referenciaProducto, B.subnombre AS nombreAlmacen, A.valor AS valorProducto FROM producto AS A LEFT JOIN almacen AS B ON A.id_almacen = B.id_almacen WHERE A.estado = '1' AND A.palabras_clave LIKE '%$producto%' AND B.id_almacen = '$almacen';"); 
                                while ($producto =(mysqli_fetch_array($selectProducto))) {

                                $idProducto = $producto['idProducto'];
                                $nombreProducto = $producto['nombreProducto'];
                                $marcaProducto = $producto['marcaProducto'];
                                $descripcionProducto = $producto['descripcionProducto'];
                                $referenciaProducto = $producto['referenciaProducto'];
                                $nombreAlmacen = $producto['nombreAlmacen'];
                                $AccionProducto = "accion";    ?>

                                <tr role='row'>
                                    <td>
                                        <center>
                                        <button type="button" class="btn btn-outline-info btn-sm" onClick="javascript:ventanaSecundaria2('../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ventPoductAndPromo.php?xnfgtiyyjk=<?php echo base64_encode($idProducto)?>')">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        </center>
                                    </td>
                                    <td><?php echo $nombreProducto; ?></td>
                                    <td><?php echo $marcaProducto; ?></td>
                                    <td><?php echo $descripcionProducto; ?></td>
                                    <td><?php echo $referenciaProducto; ?></td>
                                    <td><?php echo $nombreAlmacen; ?></td>
                                    <td><?php echo $AccionProducto; ?></td>
                                </tr>   <?php  
                                }
                                ?>   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- scritp -->
            <script>
                $(document).ready(function () {
                    $('#myTable').DataTable();
                });
            </script>                    
            <?php
            }
            if ($promoAlmacen != '') 
            {
            ?>
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">   
                    <div class="x_title">
                        <h2>Promociones del almacen </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="myTable2" class="table table-striped table-bordered col-sm-12" style="width:100%">
                            <thead>
                            <tr>
                                <th>Sugerir</th>
                                <th>Promociones</th>
                                <th>Fecha inicio</th>
                                <th>Fecha Fin</th>
                                <th>Fecha registro</th>
                            </tr>
                            </thead>

                            <tbody>
                            
                                <?php   $selectPromoAlmacen = mysqli_query($conex,"SELECT id_promocion,nombre_promocion,tipo_promocion,cod_referencia,descripcion,fecha_registro,fecha_ini,fecha_fin FROM promocion WHERE id_almacen = '$promoAlmacen';"); 
                                while ($proAlmacen =(mysqli_fetch_array($selectPromoAlmacen))) {
                                
                                $id_promocion = $proAlmacen['id_promocion'];
                                $nombrePromocion = $proAlmacen['nombre_promocion'];
                                $tipoPromocion = $proAlmacen['tipo_promocion'];
                                $referenciaPromocion = $proAlmacen['cod_referencia'];
                                $descripcionPromocion = $proAlmacen['descripcion'];
                                $fechaRegisPromocion = $proAlmacen['fecha_registro'];
                                $fechaInicioPromocion = $proAlmacen['fecha_ini'];
                                $fechaFinPromocion = $proAlmacen['fecha_fin'];   

                                if ($tipoPromocion == '1') {
                                $tipoPromocion = 'Demostracion y muestras';
                                }else if ($tipoPromocion == '2') {
                                $tipoPromocion = 'Cupones o puntos';
                                }else if ($tipoPromocion == '3') {
                                $tipoPromocion = 'Descuentos';
                                }else if ($tipoPromocion == '4') {
                                $tipoPromocion = 'Productos gratuitos';
                                }else if ($tipoPromocion == '5') {
                                $tipoPromocion = 'Combos';
                                }else if ($tipoPromocion == '6') {
                                $tipoPromocion = 'Especiales';
                                } 
                                ?>
                                
                                <tr>
                                    <td>
                                    <center>
                                    <button type="button" class="btn btn-outline-info btn-sm" onClick="javascript:ventanaSecundaria2('../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ventPoductAndPromo.php?xnfgtiyy=<?php echo base64_encode($id_promocion)?>')">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                    </center>
                                    </td>
                                    <td>
                                    <li class="media event">
                                        <a class="pull-left border-aero profile_thumb"><i class="fa fa-tag aero"></i></a>
                                        <div class="media-body">
                                            <span id="sizeFont">
                                                <a class="title" href="#"> <?php echo $nombrePromocion." - ".$tipoPromocion ?></a>
                                                <span> <?php echo $referenciaPromocion." ".$descripcionPromocion; ?></span>
                                            </span>
                                        </div>
                                    </li> 
                                    </td>
                                    <td><span id="sizeFont"><?php echo $fechaInicioPromocion ?></span></td>
                                    <td><span id="sizeFont"><?php echo $fechaFinPromocion ?></span></td>
                                    <td><span id="sizeFont"><?php echo $fechaRegisPromocion ?></span></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php    
        }
        if ($productoNuevo != '') {
            ?>
            <div class="col-md-12 col-sm-12">
            <br><br>
                <div class="x_panel">   
                    <div class="x_title">
                        <h2> Nuevo producto </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class=""><i>&nbsp;</i></a></li>
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                                <label for="">Categorias</label>
                                <select class="form-control select2_v2 select2-danger" data-dropdown-css-class="select2-danger" id="categoriasPro" style="width: 100%;">
                                    <option selected value="">selecionar...</option>
                                    <?php
                                    $selectCategoria=mysqli_query($conex,"SELECT * FROM producto_categoria WHERE estado='1';");
                                    while ($categoria =(mysqli_fetch_array($selectCategoria))) { ?>
                                    <option value="<?php echo $categoria['id_categoria']; ?>"><?php echo $categoria['nombre']; ?></option>
                                    <?php 
                                    } ?>
                                </select> 
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div id="returnSubCategoria">
                                <label for="">Subcategorias</label>
                                    <select name="selectSubCategoria" id="selectSubCategoria" class="form-control" >
                                        <option value="">Selecionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label for="">Almacen</label>
                                <select class="form-control select2_v2 select2-danger" data-dropdown-css-class="select2-danger" id="buscarAlmacen2" style="width: 100%;">
                                    <option selected value="">selecionar...</option>
                                    <?php
                                    $selectAlmacen2=mysqli_query($conex,"SELECT * FROM almacen WHERE estado='1';");
                                    while ($almacen2 =(mysqli_fetch_array($selectAlmacen2))) { ?>
                                    <option value="<?php echo $almacen2['id_almacen']; ?>"><?php echo $almacen2['subnombre']." ".$almacen2['direccion']; ?></option>
                                    <?php 
                                    } ?>
                                </select> 
                            </div>
                        </div>
                        <div class="row col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                                <label for="">referencia del producto</label>
                                <input type="text" name="referenciaPro" id="referenciaPro" class="form-control" placeholder="">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label for="">marca</label>
                                <input type="text" name="marcaPro" id="marcaPro" class="form-control" placeholder="">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <label for="">Nombre producto</label>
                                <input type="text" name="nombrePro" id="nombrePro" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="row col-md-12 col-sm-12">
                            <div class="col-md-9 col-sm-9">
                                <label for="">Descripcion</label>
                                <textarea name="descripcionPro" id="descripcionPro" class="form-control" placeholder=""></textarea>
                            </div> 
                            <div class="col-md-3 col-sm-3">
                            <label>&nbsp;</label>
                                <center><button type="button" id="agregarProductoNuevo" class="btn btn-success btn-sm">Agregar</button></center>
                               <label></label>
                            </div>  
                            <div id="returnIngresaProductoNuevo"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <!-- Datatables --> 
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <!-- agregar_productos -->
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/agregarProductos.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
        
    </body>
</html>
<?php
}else {
echo 'usted no tiene permisos';
}
?>
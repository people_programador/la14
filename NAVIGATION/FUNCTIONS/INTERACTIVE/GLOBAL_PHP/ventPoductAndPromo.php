<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>promocion</title>
        <!-- Bootstrap -->
        <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
        <!-- Custom Theme Style -->
        <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
        <!-- Font Awesome -->
        <link href="../../../DESING/BOOKSTORES/FONT/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <?php 
    require ('../../../CONNECTION/SECURITY/conex.php');
    ?>
    <style>
        .container{
            margin-top:6%;
        }
        .img-fluid {
            max-width: 50%;
            height: auto;
        }
        .thumbnail .image {
            height: auto;
            overflow: hidden;
        }

    </style>
    <body class="nav-md">
        <?php 
        $string_intro = getenv("QUERY STRING"); 
        parse_str($string_intro);

        if (isset($_GET['xnfgtiyy'])) {

        $id_promo = base64_decode($_GET['xnfgtiyy']);

        $selectPromoAlmacen = mysqli_query($conex,"SELECT id_promocion,nombre_promocion,tipo_promocion,img_promocion,cod_referencia,descripcion,fecha_registro,fecha_ini,fecha_fin FROM promocion WHERE id_promocion = '$id_promo';"); 
        while ($proAlmacen =(mysqli_fetch_array($selectPromoAlmacen))) {

        $id_promocion = $proAlmacen['id_promocion'];
        $nombrePromocion = $proAlmacen['nombre_promocion'];
        $tipoPromocion = $proAlmacen['tipo_promocion'];
        $img_promocion = $proAlmacen['img_promocion'];   
        $referenciaPromocion = $proAlmacen['cod_referencia'];
        $descripcionPromocion = $proAlmacen['descripcion'];
        $fechaRegisPromocion = $proAlmacen['fecha_registro'];
        $fechaInicioPromocion = $proAlmacen['fecha_ini'];
        $fechaFinPromocion = $proAlmacen['fecha_fin'];   

        if ($tipoPromocion == '1') {
        $tipoPromocion = 'Demostracion y muestras';
        }else if ($tipoPromocion == '2') {
        $tipoPromocion = 'Cupones o puntos';
        }else if ($tipoPromocion == '3') {
        $tipoPromocion = 'Descuentos';
        }else if ($tipoPromocion == '4') {
        $tipoPromocion = 'Productos gratuitos';
        }else if ($tipoPromocion == '5') {
        $tipoPromocion = 'Combos';
        }else if ($tipoPromocion == '6') {
        $tipoPromocion = 'Especiales';
        } 
        } 
        ?>
        <div class="container">
            <div class="row">
                <div class="x_panel">
                    <form action="../../../FUNCTIONS/CRUD/productAndPromoIndividual.php" method="post">
                        <input type="hidden" name="tipo" value="1">
                        <input type="hidden" name="promo" value="<?php echo $id_promo ?>">
                        <div class="x_title">
                            <span><h2><?php echo $nombrePromocion."-".$tipoPromocion; ?></h2><center><button class="btn btn-round btn-success" type="submit">Sugerir Promocion</button></center></span>
                        </div>
                        <div class="x_content">
                            <div class="row col-md-12 col-sm-12">
                                <div class="col-md-55"><!-- la imagen solo se puede adatar con la siguiente linea col-md-55 -->
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            <img style="width: 100%; height:auto; display: block;" src="../../../DESING/IMG/PROMOCIONES/<?php echo $img_promocion;?>" class="img-fluid" alt="Responsive image" />
                                        </div>
                                       
                                    </div>
                                </div> 
                                <div class="col-md-6 col-sm-6">
                                    <p>Informacion de la promocion</p>
                                    <ul>
                                        <li><?php echo "Referencia: ".$referenciaPromocion ?></li>
                                        <li><?php echo "descripcion: ".$descripcionPromocion ?></li>
                                        <li><?php echo "Lanzamiento: ".$fechaRegisPromocion ?></li>
                                        <li><?php echo "Desde: ".$fechaInicioPromocion ?></li>
                                        <li><?php echo "Hasta: ".$fechaFinPromocion ?></li>
                                    </ul>
                                </div>
                            </div>       
                        </div>
                        <div class="col-md-12 col-sm-12">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
        }
        if (isset($_GET['xnfgtiyyjk'])) {

        $id_producto = base64_decode($_GET['xnfgtiyyjk']);

        $ProductoIndividual = mysqli_query($conex,"SELECT C.nombre AS nombreCategoria, B.nombre AS nombreSubCategoria, D.subnombre AS nombreAlmacen, A.nombre_pro AS nombreProducto, A.marca AS marcaPorducto, A.referencia_pro AS referenciaProducto, A.descripcion AS descripcionProducto, A.img_producto AS imgProducto FROM producto AS A LEFT JOIN producto_subcategoria AS B ON B.id_subcategoria = A.id_subcategoria LEFT JOIN producto_categoria AS C ON B.id_categoria = C.id_categoria LEFT JOIN almacen AS D ON D.id_almacen = A.id_almacen WHERE A.estado = '1' AND A.id_producto = '$id_producto';"); 
        while ($producto =(mysqli_fetch_array($ProductoIndividual))) {

        $nombreCategoria = $producto['nombreCategoria']; 
        $nombreSubCategoria = $producto['nombreSubCategoria'];
        $nombreAlmacen = $producto['nombreAlmacen'];
        $nombreProducto = $producto['nombreProducto'];   
        $marcaPorducto = $producto['marcaPorducto'];
        $referenciaProducto = $producto['referenciaProducto'];
        $descripcionProducto = $producto['descripcionProducto'];
        $imgProducto = $producto['imgProducto'];
        }                     
        ?>
        <div class="container">
            <div class="row">
            <div class="x_panel">
                    <form action="../../../FUNCTIONS/CRUD/productAndPromoIndividual.php" method="post">
                        <input type="hidden" name="tipo" value="2">
                        <input type="hidden" name="producto" value="<?php echo $id_producto ?>">
                        <div class="x_title">
                            <span><h2><?php echo $nombreProducto; ?></h2><center><button class="btn btn-round btn-success" type="submit">Agregar Producto</button></center></span>
                        </div>
                        <div class="x_content">
                            <div class="row col-md-12 col-sm-12">
                                <div class="col-md-55"><!-- la imagen solo se puede adatar con la siguiente linea col-md-55 -->
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            <img src="../../../DESING/IMG/PRODUCTOS/<?php echo $imgProducto;?>" style="width: 100%; height:auto; display: block;" class="img-fluid" alt="Responsive image">         
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6 col-sm-6">
                                    <p>Informacion del Producto</p>
                                    <ul>
                                        <li><?php echo "categoria: ".$nombreCategoria ?></li>
                                        <li><?php echo "subcategoria: ".$nombreSubCategoria ?></li>
                                        <li><?php echo "Almacen: ".$nombreAlmacen ?></li>
                                        <li><?php echo "Nombre: ".$nombreProducto ?></li>
                                        <li><?php echo "Marca: ".$marcaPorducto ?></li>
                                        <li><?php echo "Referencia: ".$referenciaProducto ?></li>
                                        <li><?php echo "Descripcion: ".$descripcionProducto ?></li>
                                    </ul>
                                </div>
                            </div>       
                        </div>
                        <div class="col-md-12 col-sm-12">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
        }else {
        //echo "";
        }
        ?>
        <!-- Bootstrap -->
        <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
    </body>
</html>
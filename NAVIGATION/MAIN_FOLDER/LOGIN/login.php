<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>La 14</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/../../DESING/IMG/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="../../DESING/BOOKSTORES/PACKAGE/login/css/main.css">
<!--===============================================================================================-->
</head>
<body class="login">
	
	
  <div class="container-login100" style="background-image: url('../../DESING/IMG/1.jpg');">
  <form action="../../FUNCTIONS/CRUD/ingreso_temp.php" method="post">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
			<form class="login100-form validate-form">
				
				<span class="login100-form-title p-b-37">
					<span style="color:#bb191f">People</span><span style="color:#2a3f54;">Call</span>Service				</span>
 
				<div class="wrap-input100 validate-input m-b-20" data-validate="Ingrese Usuario">
					<input name="RW1haWw=" id="RW1haWw=" class="input100" type="text" autocomplete="off" placeholder="Usuario" required ="required">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input m-b-25" data-validate = "Ingrese Contrase&ntilde;a">
					<input name="UGFzc3dvcmQ=" id="UGFzc3dvcmQ" class="input100" autocomplete="off" type="password" placeholder="Contrase&ntilde;a" required = "required">
					<span class="focus-input100"></span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						Ingresar
						
					</button>
					
				</div>

				<br>

				
				
				<div class="text-center">
					
						<img src="../../DESING/BOOKSTORES/PACKAGE/login/images/Logo People.png"  width="170" >
					
				</div>
			</form>

			
    </div>
  </form>
	</div>
	
	

	
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/bootstrap/js/popper.js"></script>
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../../DESING/BOOKSTORES/PACKAGE/login/js/main.js"></script>

</body>
</html>
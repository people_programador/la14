<?php
require ('../../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
    require ('../../../CONNECTION/SECURITY/conex.php');
    // require php
    ?> 
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
           
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
          
            
            <title>Administrador</title>
            
						<link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
            <!-- Bootstrap -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
            <!-- Font Awesome -->
            <link href="../../../DESING/BOOKSTORES/FONT/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/iCheck/skins/flat/green.css" rel="stylesheet">
            <!-- bootstrap-wysiwyg -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
            <!-- Select2 -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/css/select2.min.css" rel="stylesheet">
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2-bootstrap4-theme/select2-bootstrap4.min.css" rel="stylesheet">
            <!-- Switchery -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.css" rel="stylesheet">
            <!-- starrr -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/starrr/dist/starrr.css" rel="stylesheet">
            <!-- bootstrap-daterangepicker -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
			 			<link href="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
			 			<link href="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
			  		<link href="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
			     	<link href="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
				 
            <!-- Custom Theme Style -->
            <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">
						<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
						<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
            <script src="../../../DESING/JS/jquery.js"></script>
						<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/datosPerdonales.js"></script>
			
 
        </head>

        <body class="nav-md">
            <!--  .container body -->
            <div class="container body">
                <!--  .main_container -->
                <div class="main_container">
                    <?php require ('../../DROPDOWM/ADMIN/menu_admin.php');?>
                </div>
                <!--  .main_container -->
                <div class="right_col" role="main">  
                  <div class="row">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Default Example <small>Users</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Settings 1</a>
                                <a class="dropdown-item" href="#">Settings 2</a>
                              </div>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                              <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                  </tr>
                                </thead>

                                <tbody>
                                  <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>2011/04/25</td>
                                    <td>$320,800</td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- footer content -->
                <footer>
                <div class="pull-right">
                    People Marketing SAS
                </div>
                <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>     
             <!--  /.container body -->   

            <!-- Bootstrap -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
            <!-- FastClick -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/nprogress.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/iCheck/icheck.min.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/moment/min/moment.min.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap-wysiwyg -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
			
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.hotkeys/jquery.hotkeys.js"></script>
			
            <script src="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/src/prettify.js"></script>
            <!-- jQuery Tags Input -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/jquery.tagsinput/src/jquery.tagsinput.js"></script>
            <!-- Switchery -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.js"></script>
            <!-- Select2 -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/js/select2.full.min.js"></script>
            <!-- Parsley -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/parsleyjs/dist/parsley.min.js"></script>
            <!-- Autosize -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/autosize.min.js"></script> <!-- complemento.min.js-->
            <!-- jQuery autocomplete -->
			
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net/js/jquery.dataTables.min.js"></script>
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        
            <script src="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>	
            <script src="../../../DESING/BOOKSTORES/PACKAGE/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

			      <!-- starrr -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/starrr/dist/starrr.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
            <!-- interactive js -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/typing.js"></script>
        </body>
    </html>
    <?php 
	
	
	
}else { 
    echo 'usted no tiene permisos';
}
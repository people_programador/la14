<?php
require ('../../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
    require ('../../../CONNECTION/SECURITY/conex.php');
    // require php
    ?> 
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <title>Formulario Asesor</title>

            <!-- Bootstrap -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
            <!-- Font Awesome -->
            <link href="../../../DESING/BOOKSTORES/FONT/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/iCheck/skins/flat/green.css" rel="stylesheet">
            <!-- bootstrap-wysiwyg -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
            <!-- Select2 -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/css/select2.min.css" rel="stylesheet">
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2-bootstrap4-theme/select2-bootstrap4.min.css" rel="stylesheet">
            <!-- Switchery -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.css" rel="stylesheet">
            <!-- starrr -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/starrr/dist/starrr.css" rel="stylesheet">
            <!-- bootstrap-daterangepicker -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

            <!-- Custom Theme Style -->
            <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">
        </head>
        <body class="nav-md">
            <!--  .container body -->
            <div class="container body">
                <!--  .main_container -->
                <div class="main_container">
                    <?php require ('../../DROPDOWM/ADVISER/menu_adviser.php');?>
                </div>
                <!--  .main_container -->
                <div class="right_col" role="main">
                  <div class="x_title">
                    <h2>Tipificaci&oacute;n</h2>
                    <br>
                  </div>
                  <div class="x_panel">
                  <div class="x_content">
                    <br />
                    <form id="busqueda_cliente" name="busqueda_cliente" method="POST" acti data-parsley-validate class="form-horizontal form-label-left">
                      <div class="x_title">
                        <div class="form-group row ">
                          <h2 class="control-label col-md-6 col-sm-6 ">La 14 servicio al cliente Call</h2>
                          <div class="col-md-3 col-sm-3 form-group row pull-right top_search">
                          <div class="input-group">
                              <input type="text" class="form-control" >
                              <span class="input-group-btn">
                                <button class="btn btn-secondary" name="cliente_telefono" id="cliente_telefono" type="button">Telefono</button>
                              </span>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3 form-group row pull-right top_search">
                            <div class="input-group">
                              <input type="text" class="form-control" >
                              <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Cedula</button>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br />
                    </form>
                    <?php
                        if (isset(_POST['cliente_telefono'])) {
                            $telefono = $_POST['cliente_telefono'];
                            $cliente=mysqli_query($conex,"SELECT * FROM cliente_info WHERE telefono='$telefono';");

                            while ($almacen =(mysqli_fetch_array($cliente))) { ?>

                                <option value="<?php echo $almacen['id_almacen']; ?>"><?php echo $almacen['subnombre']." ".$almacen['direccion']; ?></option>

                            <?php 
                            } 
                        } else {
                            Echo "Se ha pulsado el botón cancelar";
                        }
                    ?>
                    <form class="form-label-left input_mask">

                      <div class="col-md-3 col-sm-3  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Nombres">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-3 col-sm-3  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Apellidos">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-3 col-sm-3  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Email">
                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-3 col-sm-3  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Cedula">
                        <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Direcci&oacute;n">
                        <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-4 col-sm-4  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Ciudad">
                        <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      
                      <div class="col-md-4 col-sm-4  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="" placeholder="Tel&eacute;fono">
                        <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <br>
                      <br>
                      <br>
                      <br>
                      <br>
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <div class="col-md-9 col-sm-9  offset-md-4">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                    </form>
                    
                    <div class="x_title">
                      <div class="row">
                          <br>
                          <h2 class="control-label col-md-12 col-sm-12 ">PQRS</h2>
                      </div>
                    </div>
                    <br>
                    <form id="pqrs" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Seleccione Opci&oacute;n</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select class="form-control">
                            <option>Seleccionar...</option>
                            <option>Peticiones</option>
                            <option>Quejas</option>
                            <option>Reclamos</option>
                            <option>Sugerencias</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observaci&oacute;n</label>
                        <div class="col-md-6 col-sm-6 ">
                          <textarea class="form-control" rows="3" placeholder="Resumen del PQRS"></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <div class="col-md-6 col-sm-6 offset-md-4">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                    </form>

                    <div class="x_title">
                      <div class="row">
                          <br>
                          <h2 class="control-label col-md-12 col-sm-12 ">Promociones</h2>
                      </div>
                    </div>
                    <form id="pqrs" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="row col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6  form-group pull-right top_search">    
                                <label for="Almacenes" class="col-form-label col-md-6 col-sm-6">Almacen</label>
                                <div class="input-group">
                                <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" id="Almacenes" style="width: 80%;">
                                    <option selected value="">selecionar...</option>
                                    <?php
                                        $selectAlmacen=mysqli_query($conex,"SELECT * FROM almacen WHERE estado='1';");

                                        while ($almacen =(mysqli_fetch_array($selectAlmacen))) { ?>

                                            <option value="<?php echo $almacen['id_almacen']; ?>"><?php echo $almacen['subnombre']." ".$almacen['direccion']; ?></option>

                                        <?php 
                                        } 
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-secondary source" id="BuscarAlmacen">Buscar</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6">
                                <div class="x_panel">   
                                    <div class="x_title">
                                        <h2>Promociones Añadidas <?php echo $nombreAlmacen?></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="row col-md-12 col-sm-12">
                                            <div class="col-md-6 col-sm-6">
                                            <li class="media event">
                                                <a class="pull-left border-blue profile_thumb">
                                                <i class="fa fa-tag blue"></i>
                                                </a>
                                                <div class="media-body">
                                                <a class="title" href="#">Promocion de aceite - Descuentos</a>
                                                <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                <p> <small>12 Sales Today</small>
                                                </p>
                                                </div>
                                            </li>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                            <li class="media event">
                                                <a class="pull-left border-green profile_thumb">
                                                <i class="fa fa-tag green"></i>
                                                </a>
                                                <div class="media-body">
                                                <a class="title" href="#">Promocion de aceite - Cupones o puntos</a>
                                                <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                <p> <small>12 Sales Today</small>
                                                </p>
                                                </div>
                                            </li>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            <div class="x_panel">   
                                    <div class="x_title">
                                        <h2>Promociones del almacen <?php echo $nombreAlmacen?></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class=""><i>&nbsp;</i></a></li>
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                    <ul class="list-unstyled top_profiles scroll-view">
                                        <li class="media event">
                                            <a class="pull-left border-aero profile_thumb">
                                            <i class="fa fa-tag aero"></i>
                                            </a>
                                            <div class="media-body">
                                            <a class="title" href="#">Promocion de aceite - Demostracion y muestras</a>
                                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                            <p> <small>12 Sales Today</small>
                                            </p>
                                            </div>
                                        </li>
                                        <li class="media event">
                                            <a class="pull-left border-green profile_thumb">
                                            <i class="fa fa-tag green"></i>
                                            </a>
                                            <div class="media-body">
                                            <a class="title" href="#">Promocion de aceite - Cupones o puntos</a>
                                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                            <p> <small>12 Sales Today</small>
                                            </p>
                                            </div>
                                        </li>
                                        <li class="media event">
                                            <a class="pull-left border-blue profile_thumb">
                                            <i class="fa fa-tag blue"></i>
                                            </a>
                                            <div class="media-body">
                                            <a class="title" href="#">Promocion de aceite - Descuentos</a>
                                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                            <p> <small>12 Sales Today</small>
                                            </p>
                                            </div>
                                        </li>
                                        <li class="media event">
                                            <a class="pull-left border-aero profile_thumb">
                                            <i class="fa fa-tag aero"></i>
                                            </a>
                                            <div class="media-body">
                                            <a class="title" href="#">Promocion de aceite - productos gratuitos</a>
                                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                            <p> <small>12 Sales Today</small>
                                            </p>
                                            </div>
                                        </li>
                                        <li class="media event">
                                            <a class="pull-left border-green profile_thumb">
                                            <i class="fa fa-tag green"></i>
                                            </a>
                                            <div class="media-body">
                                            <a class="title" href="#">Promocion de aceite - especiales</a>
                                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                            <p> <small>12 Sales Today</small>
                                            </p>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                  </div>
                </div>
                <!-- footer content -->
                <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>     
             <!--  /.container body -->   

            <!-- scritp -->
            <!-- jQuery -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
            <!-- FastClick -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/nprogress.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/iCheck/icheck.min.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/moment/min/moment.min.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap-wysiwyg -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.hotkeys/jquery.hotkeys.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/src/prettify.js"></script>
            <!-- jQuery Tags Input -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/jquery.tagsinput/src/jquery.tagsinput.js"></script>
            <!-- Switchery -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.js"></script>
            <!-- Select2 -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/js/select2.full.min.js"></script>
            <!-- Parsley -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/parsleyjs/dist/parsley.min.js"></script>
            <!-- Autosize -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/autosize.min.js"></script> <!-- complemento.min.js-->
            <!-- jQuery autocomplete -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
            <!-- starrr -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/starrr/dist/starrr.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
            <!-- interactive js -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/typing.js"></script>
        </body>
    </html>
    <?php
}else {
    echo 'usted no tiene permisos';
}
<?php
require ('../../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
    require ('../../../CONNECTION/SECURITY/conex.php');
    // require php
    ?> 
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <title>Gentelella Alela! | </title>

            <!-- Bootstrap -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
            <!-- Font Awesome -->
            <link href="../../../DESING/BOOKSTORES/FONT/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/iCheck/skins/flat/green.css" rel="stylesheet">
            <!-- bootstrap-wysiwyg -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
            <!-- Select2 -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/css/select2.min.css" rel="stylesheet">
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2-bootstrap4-theme/select2-bootstrap4.min.css" rel="stylesheet">
            <!-- Switchery -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.css" rel="stylesheet">
            <!-- starrr -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/starrr/dist/starrr.css" rel="stylesheet">
            <!-- bootstrap-daterangepicker -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
            <!-- Custom Theme Style -->
            <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">
  
        </head>
        
        <body class="nav-md">
            <!--  .container body -->
            <div class="container body">
                <!--  .main_container -->
                <div class="main_container">
                    <?php require ('../../DROPDOWM/ADVISER/menu_adviser.php');?>
                </div>
                <!--  /.main_container -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                        <div class="title_left">
                            <h3>Promociones Por Almacen</h3>
                        </div>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3  form-group pull-right top_search">
                            </div>
                            <div class="col-md-6 col-sm-6  form-group pull-right top_search">    
                                <div class="input-group">
                                <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" id="Almacenes" style="width: 80%;">
                                    <option selected value="">selecionar...</option>
                                    <?php
                                        $selectAlmacen=mysqli_query($conex,"SELECT * FROM almacen WHERE estado='1';");

                                        while ($almacen =(mysqli_fetch_array($selectAlmacen))) { ?>

                                            <option value="<?php echo $almacen['id_almacen']; ?>"><?php echo $almacen['subnombre']." ".$almacen['direccion']; ?></option>

                                        <?php 
                                        } 
                                    ?>
                                </select>
                                    <span class="input-group-btn">
                                    <button class="btn btn-default source" id="BuscarAlmacen">Buscar</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3  form-group pull-right top_search">
                            </div>
                        </div>
                        <div class="row" id="returnConsultarAlmacen">  
                                  
                        </div>
                        <div class="row">
                        
                        </div>
                    </div>

                </div>
                </div>
                <!-- footer content -->
                <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>     
            <!--  /.container body -->   

            <!-- scritp -->
            <!-- jQuery -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
            <!-- FastClick -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/nprogress.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/iCheck/icheck.min.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/moment/min/moment.min.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap-wysiwyg -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.hotkeys/jquery.hotkeys.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/src/prettify.js"></script>
            <!-- jQuery Tags Input -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/jquery.tagsinput/src/jquery.tagsinput.js"></script>
            <!-- Switchery -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.js"></script>
            <!-- Parsley -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/parsleyjs/dist/parsley.min.js"></script>
            <!-- Autosize -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/autosize.min.js"></script> <!-- complemento.min.js-->
            <!-- jQuery autocomplete -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
            <!-- starrr -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/starrr/dist/starrr.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
            <!-- Select2 -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/js/select2.full.min.js"></script>
            <!-- interactive js -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/promotions_warehouse.js"></script>
        </body>
    </html>
    <?php
}else {
    echo 'usted no tiene permisos';
}
<?php
require ('../../../CONNECTION/SECURITY/session.php');
if ($user_name != '') {
    require ('../../../CONNECTION/SECURITY/conex.php');
    // require php
    ?> 
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            
            <title>Formulario Asesor</title>

            <!-- Bootstrap -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
            <!-- Font Awesome -->
            <link href="../../../DESING/BOOKSTORES/FONT/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <!-- NProgress -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/nprogress/nprogress.css" rel="stylesheet">
            <!-- iCheck -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/iCheck/skins/flat/green.css" rel="stylesheet">
            <!-- bootstrap-wysiwyg -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
            <!-- Select2 -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/css/select2.min.css" rel="stylesheet">
            <link href="../../../DESING/BOOKSTORES/PACKAGE/select2-bootstrap4-theme/select2-bootstrap4.min.css" rel="stylesheet">
            <!-- Switchery -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.css" rel="stylesheet">
            <!-- starrr -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/starrr/dist/starrr.css" rel="stylesheet">
            <!-- bootstrap-daterangepicker -->
            <link href="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

            <!-- Custom Theme Style -->
            <link href="../../../DESING/CSS/custom.min.css" rel="stylesheet">

            <!-- direccion js -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/direccion.js"></script>
            <!-- script de direccion -->
            <script>
              $(document).ready(function()
              {
                var via=$('#VIA').val();
                var dt_via=$('#detalle_via').val();
                $('#VIA').change(function()
                {
                  dir();
                });
                
                $('#detalle_via').change(function()
                {
                  dir();
                });
                $('#numero').change(function()
                {
                  dir();
                });
                $('#numero2').change(function()
                {
                  dir();
                });
                $('#interior').change(function()
                {
                  dir();		
                });
                $('#detalle_int').change(function()
                {
                  dir();
                });
                $('#interior2').change(function()
                {
                  dir();		
                });
                $('#detalle_int2').change(function()
                {
                  dir();
                });
                $('#interior3').change(function()
                {
                  dir();		
                });
                $('#detalle_int3').change(function()
                {
                  dir();
                });                
              });
            </script>
        </head>
        <body class="nav-md">
            <!--  .container body -->
            <div class="container body">
                <!--  .main_container -->
                <div class="main_container">
                    <?php require ('../../DROPDOWM/ADVISER/menu_adviser.php');?>
                </div>
                <!--  .main_container -->
                <div class="right_col" role="main">  
                  <div class="row">
                        <div class="x_title col-md-12 col-sm-12">
                          <form id="busqueda_cliente" name="busqueda_cliente" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group row ">
                              <h3 class="control-label col-md-6 col-sm-6 ">  Servicio al cliente Call</h3>
                              <div class="col-md-3 col-sm-3 form-group row pull-right top_search"><br>
                                <div class="input-group">
                                  <input type="text" name="cliente_telefono" id="cliente_telefono" class="form-control" >
                                  <span class="input-group-btn">
                                    <button class="btn btn-success" name="btn_telefono" id="btn_telefono" style="color:#ffffff;" type="submit">Telefono</button>
                                  </span>
                                </div>
                              </div>
                              <div class="col-md-3 col-sm-3 form-group row pull-right top_search">
                                <div class="input-group">
                                  <input type="text" class="form-control" >
                                  <span class="input-group-btn">
                                    <button class="btn btn-success" style="color:#ffffff;" type="button">Cedula</button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      <?php
                        $telefono_bus = $_POST['cliente_telefono'];
                        echo $telefono_bus;
                          $sql_telefono=mysqli_query($conex,"SELECT * FROM  la_catorce.cliente_info WHERE telefono='$telefono_bus';");
                          echo "<br>"."SELECT * FROM  la_catorce.cliente_info WHERE telefono='$telefono_bus';";
                          while ($datos =(mysqli_fetch_array($sql_telefono))) { 
                            $nombre = $datos['nombre'];
                            $apellido = $datos['apellido'];
                            $email = $datos['correo'];
                            $cedula = $datos['cedula'];
                            $direccion = $datos['direccion'];
                            $ciudad = $datos['id_ciudad'];
                            $telefono = $datos['telefono'];
                          }
                        echo "<br>".$nombre;
                      ?>
                    <div class="col-md-12 col-sm-12">
                      <div class="x_panel">
                        
                        <div class="x_content">
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel">
                              <div class="x_title">
                                <h2><i class="fa fa-align-left"></i> Collapsible / Accordion <small>Sessions</small></h2>
                                
                                <div class="clearfix"></div>
                              </div>
                                <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="panel-title">Datos Cliente</h4>
                                </a>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                  <form class="form-label-left input_mask">
                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $nombre ?>" id="" placeholder="Nombres">
                                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $apellido ?>" id="" placeholder="Apellidos">
                                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $email ?>" id="" placeholder="Email">
                                      <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $email ?>" id="" placeholder="Cedula">
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $email ?>" id="" placeholder="Tel&eacute;fono">
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-4 col-sm-4  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" value="<?php echo $email ?>" id="" placeholder="Ciudad">
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-12 col-sm-12  form-group has-feedback">
                                      <input type="text" class="form-control has-feedback-left" placeholder="Direcci&oacute;n" name="DIRECCION" id="DIRECCION" />
                                      <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
                                      <br>
                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Via:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="VIA" name="VIA" class="form-control">
                                              <option value="">Seleccione...</option>
                                              <option>ANILLO VIAL</option>
                                              <option>AUTOPISTA</option>
                                              <option>AVENIDA</option>
                                              <option>BOULEVAR</option>
                                              <option>CALLE</option>
                                              <option>CALLEJON</option>
                                              <option>CARRERA</option>
                                              <option>CIRCUNVALAR</option>
                                              <option>CONDOMINIO</option>
                                              <option>DIAGONAL</option>
                                              <option>KILOMETRO</option>
                                              <option>LOTE</option>
                                              <option>SALIDA</option>
                                              <option>SECTOR</option>
                                              <option>TRANSVERSAL</option>
                                              <option>VEREDA</option>
                                              <option>VIA</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Via:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_via" id="detalle_via" type="text" maxlength="15" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">N&uacute;mero:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="numero" id="numero" type="text" maxlength="5" class="form-control"/>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">
                                          <label style="text-align: center;">-</label>
                                        </div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="numero2" id="numero2" type="text" maxlength="5" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interior" name="interior" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_int" id="detalle_int" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interior2" name="interior2" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_int2" id="detalle_int2" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>

                                      <div class="col-md-12 col-sm-12 "></div>

                                      <div class="col-md-12 col-sm-12 ">
                                        <div class="col-md-2 col-sm-2 ">Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <select id="interior3" name="interior3" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <option>APARTAMENTO</option>
                                            <option>BARRIO</option>
                                            <option>BLOQUE</option>
                                            <option>CASA</option>
                                            <option>CIUDADELA</option>
                                            <option>CONJUNTO</option>
                                            <option>CONJUNTO RESIDENCIAL</option>
                                            <option>EDIFICIO</option>
                                            <option>ENTRADA</option>
                                            <option>ETAPA</option>
                                            <option>INTERIOR</option>
                                            <option>MANZANA</option>
                                            <option>NORTE</option>
                                            <option>OFICINA</option>
                                            <option>OCCIDENTE</option>
                                            <option>ORIENTE</option>
                                            <option>PENTHOUSE</option>
                                            <option>PISO</option>
                                            <option>PORTERIA</option>
                                            <option>SOTANO</option>
                                            <option>SUR</option>
                                            <option>TORRE</option>
                                          </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">Detalles Interior:</div>
                                        <div class="col-md-4 col-sm-4 ">
                                          <input name="detalle_int3" id="detalle_int3" type="text" maxlength="30" class="form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="ln_solid"></div>
                                    <div class="form-group row">
                                      <div class="col-md-9 col-sm-9  offset-md-4">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">PQRS</h4>
                                </a>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                  <form id="pqrs" data-parsley-validate class="form-horizontal form-label-left">
                                    <div class="item form-group">
                                      <label class="col-form-label col-md-3 col-sm-3 label-align">Seleccione Opci&oacute;n</label>
                                      <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control">
                                          <option>Seleccionar...</option>
                                          <option>Peticiones</option>
                                          <option>Quejas</option>
                                          <option>Reclamos</option>
                                          <option>Sugerencias</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="item form-group">
                                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observaci&oacute;n</label>
                                      <div class="col-md-6 col-sm-6 ">
                                        <textarea class="form-control" rows="3" placeholder="Resumen del PQRS"></textarea>
                                      </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="item form-group">
                                      <div class="col-md-6 col-sm-6 offset-md-4">
                                        <button type="submit" class="btn btn-success">Guardar</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="panel-title">Promociones</h4>
                                </a>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                <form id="pqrs" data-parsley-validate class="form-horizontal form-label-left">
                                  <div class="row col-md-12 col-sm-12">
                                      <div class="col-md-6 col-sm-6  form-group pull-right top_search">    
                                          <label for="Almacenes" class="col-form-label col-md-6 col-sm-6">Almacen</label>
                                          <div class="input-group">
                                          <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" id="Almacenes" style="width: 80%;">
                                              <option selected value="">selecionar...</option>
                                              <?php
                                                  $selectAlmacen=mysqli_query($conex,"SELECT * FROM almacen WHERE estado='1';");

                                                  while ($almacen =(mysqli_fetch_array($selectAlmacen))) { ?>

                                                      <option value="<?php echo $almacen['id_almacen']; ?>"><?php echo $almacen['subnombre']." ".$almacen['direccion']; ?></option>

                                                  <?php 
                                                  } 
                                              ?>
                                          </select>
                                          <span class="input-group-btn">
                                              <button class="btn btn-secondary source" id="BuscarAlmacen">Buscar</button>
                                          </span>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row col-md-12 col-sm-12">
                                      <div class="col-md-6 col-sm-6">
                                          <div class="x_panel">   
                                              <div class="x_title">
                                                  <h2>Promociones Añadidas <?php echo $nombreAlmacen?></h2>
                                                  <ul class="nav navbar-right panel_toolbox">
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                  </ul>
                                                  <div class="clearfix"></div>
                                              </div>
                                              <div class="x_content">
                                                  <div class="row col-md-12 col-sm-12">
                                                      <div class="col-md-6 col-sm-6">
                                                      <li class="media event">
                                                          <a class="pull-left border-blue profile_thumb">
                                                          <i class="fa fa-tag blue"></i>
                                                          </a>
                                                          <div class="media-body">
                                                          <a class="title" href="#">Promocion de aceite - Descuentos</a>
                                                          <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                          <p> <small>12 Sales Today</small>
                                                          </p>
                                                          </div>
                                                      </li>
                                                      </div>
                                                      <div class="col-md-6 col-sm-6">
                                                      <li class="media event">
                                                          <a class="pull-left border-green profile_thumb">
                                                          <i class="fa fa-tag green"></i>
                                                          </a>
                                                          <div class="media-body">
                                                          <a class="title" href="#">Promocion de aceite - Cupones o puntos</a>
                                                          <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                          <p> <small>12 Sales Today</small>
                                                          </p>
                                                          </div>
                                                      </li>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6">
                                      <div class="x_panel">   
                                              <div class="x_title">
                                                  <h2>Promociones del almacen <?php echo $nombreAlmacen?></h2>
                                                  <ul class="nav navbar-right panel_toolbox">
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class=""><i>&nbsp;</i></a></li>
                                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                  </ul>
                                                  <div class="clearfix"></div>
                                              </div>
                                              <div class="x_content">
                                              <ul class="list-unstyled top_profiles scroll-view">
                                                  <li class="media event">
                                                      <a class="pull-left border-aero profile_thumb">
                                                      <i class="fa fa-tag aero"></i>
                                                      </a>
                                                      <div class="media-body">
                                                      <a class="title" href="#">Promocion de aceite - Demostracion y muestras</a>
                                                      <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                      <p> <small>12 Sales Today</small>
                                                      </p>
                                                      </div>
                                                  </li>
                                                  <li class="media event">
                                                      <a class="pull-left border-green profile_thumb">
                                                      <i class="fa fa-tag green"></i>
                                                      </a>
                                                      <div class="media-body">
                                                      <a class="title" href="#">Promocion de aceite - Cupones o puntos</a>
                                                      <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                      <p> <small>12 Sales Today</small>
                                                      </p>
                                                      </div>
                                                  </li>
                                                  <li class="media event">
                                                      <a class="pull-left border-blue profile_thumb">
                                                      <i class="fa fa-tag blue"></i>
                                                      </a>
                                                      <div class="media-body">
                                                      <a class="title" href="#">Promocion de aceite - Descuentos</a>
                                                      <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                      <p> <small>12 Sales Today</small>
                                                      </p>
                                                      </div>
                                                  </li>
                                                  <li class="media event">
                                                      <a class="pull-left border-aero profile_thumb">
                                                      <i class="fa fa-tag aero"></i>
                                                      </a>
                                                      <div class="media-body">
                                                      <a class="title" href="#">Promocion de aceite - productos gratuitos</a>
                                                      <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                      <p> <small>12 Sales Today</small>
                                                      </p>
                                                      </div>
                                                  </li>
                                                  <li class="media event">
                                                      <a class="pull-left border-green profile_thumb">
                                                      <i class="fa fa-tag green"></i>
                                                      </a>
                                                      <div class="media-body">
                                                      <a class="title" href="#">Promocion de aceite - especiales</a>
                                                      <p><strong>$2300. </strong> Agent Avarage Sales </p>
                                                      <p> <small>12 Sales Today</small>
                                                      </p>
                                                      </div>
                                                  </li>
                                              </ul>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of accordion -->


                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- footer content -->
                <footer>
                <div class="pull-right">
                    Gentelella - Admin Template by <a href="https://peoplemarketing.com/inicio/">People Marketing</a>
                </div>
                <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>     
             <!--  /.container body -->   

            <!-- scritp -->
            <!-- jQuery -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
            <!-- FastClick -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/fastclick.js"></script>
            <!-- NProgress -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/nprogress.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/iCheck/icheck.min.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/moment/min/moment.min.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap-wysiwyg -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/jquery.hotkeys/jquery.hotkeys.js"></script>
            <script src="../../../DESING/BOOKSTORES/PACKAGE/google-code-prettify/src/prettify.js"></script>
            <!-- jQuery Tags Input -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/jquery.tagsinput/src/jquery.tagsinput.js"></script>
            <!-- Switchery -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/switchery/dist/switchery.min.js"></script>
            <!-- Select2 -->
            <script src="../../../DESING/BOOKSTORES/PACKAGE/select2_v2/js/select2.full.min.js"></script>
            <!-- Parsley -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/parsleyjs/dist/parsley.min.js"></script>
            <!-- Autosize -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/INDEPENDENT/autosize.min.js"></script> <!-- complemento.min.js-->
            <!-- jQuery autocomplete -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
            <!-- starrr -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/BOOKSTORES_JS/PACKAGE/starrr/dist/starrr.js"></script>
            <!-- Custom Theme Scripts -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/custom.min.js"></script>
            <!-- interactive js -->
            <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/typing.js"></script>
        </body>
    </html>
    <?php
}else {
    echo 'usted no tiene permisos';
}